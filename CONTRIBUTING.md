# Contributing

Help your fellow contributors out by...

## Summary

* [ ] **Good python**: PEP8, line_length < 130, use a linter, add docstring (bonus: doctest)
* [ ] **Good git**: commit and push to a new branch often
* [ ] **Good pytest**: check that [regression tests](https://gitlab.com/tangibleai/qary/-/pipelines) pass on your branch.
* [ ] **Thank you!**

## Good python

* [ ] PEP8 standards apply, except the convention of line length
* [ ] Maximum line length is 120 characters
* [ ] Use a linter like Anaconda in Sublime text, or `flake8` anywhere (+1 for autocorrect on save)
* [ ] Write a single line docstring first (+1 for a [doctest](/-/blob/master/src/qary/skills/glossary.py#L21)) for every new function
* [ ] Write a [unittest](/-/blob/master/tests/test_qa_bots.py#L18)

## Good git

* [ ] Only one logical change per commit
* [ ] short commit messages
* [ ] `git status` often (to help you think of a commit message)
* [ ] never push to master (+1 branch names like `fix-{bug-name}` or `feature-{feature_name}`)
* [ ] delete your local branch when it has been merged to master: `git branch --delete feature-{feature-name}` 
* [ ] Merge requests and code reviews

## Good pytest

* [ ] Check [gitlab-ci](/-/pipelines) often (when you push) 
* [ ] Add 1 [unittest](/-/blob/master/tests/test_qa_bots.py) or docstest based on your manual tests

## Thank you!
    
* [ ] Add yourself to [AUTHORS.md](/AUTHORS.md)
