{

    /*
        Autoformat files on save

        This option is disabled by default, AutoPEP8 is really slow and it get
        the file buffer read only while its working in the background.

        Use this at your own risk.
    */
    "auto_formatting": true,

    /*
        Timeout for the autoformatting tool before fail

        Note: Take into account that depending on the size of the file you
        want to autoformat, this can take a while.
    */
    "auto_formatting_timeout": 3,  // seconds

    /*
        Enable unsafe changes:
            https://github.com/hhatto/autopep8#more-advanced-usage

        Set it as 0 if you are unsure here.
    */
    "aggressive": 0,

    /*
        Do not fix the errors listed here:
        Note: By default we ignore E309 as this is not pep8 but it does
        conflict with pep257 D211.
        E123: closing bracket not indented to same level as opening bracket
        E302    expected 2 blank lines, found 0
        W291 - Remove trailing whitespace.
        W504 - line ends with binary operator (or, and)

        Also look for this file and confirm there are no conflicts based on this Anacond setting:
            "args": ["--config", "~/.config/pep8"],
    */
    "autoformat_ignore":
    [
        "E309", "W291", "W504"
    ],

    "trim_trailing_white_space_on_save": false,
    /*
        Fix only the errors listed here:
    */
    "autoformat_select":
    [
    ],

    /*
        Set the threshold limit for McCabe complexity checker.
    */
    "mccabe_threshold": 14,
    /*
        If 'outline' (default), anaconda will outline error lines.
        If 'fill', anaconda will fill the lines.
        If 'solid_underline', anaconda will draw a solid underline below regions.
        If 'stippled_underline', anaconda will draw a stippled underline below regions.
        If 'squiggly_underline', anaconda will draw a squiggly underline below regions.
        If 'none', anaconda will not draw anything on error lines.
    */
    "anaconda_linter_mark_style": "stippled_underline",

    /*
        If this is set to false, anaconda will not underline errors.
    */
    "anaconda_linter_underlines": false,

    /*
        If this is set to true, anaconda will show errors inline.
    */
    "anaconda_linter_phantoms": false,

    /*
        This determines what theme is phantoms used.
    */
    "anaconda_linter_phantoms_theme": "phantom",

    /*
        This determines what template is phantoms used.
    */
    "anaconda_linter_phantoms_template": "default",

    /*
        If anaconda_linter_show_errors_on_save is set to true, anaconda
        will show a list of errors when you save the file.

        This is disabled by default.
    */
    "anaconda_linter_show_errors_on_save": true,

    /*
        If true, anaconda does not remove lint marks while you type.
    */
    "anaconda_linter_persistent": false,

    // If true, anaconda draws gutter marks on line with errors.
    "anaconda_gutter_marks": true,

    /*

        If anaconda_gutter_marks is true, this determines what theme is used.
        Theme 'basic' only adds dots and circles to gutter.

        Other available themes are 'alpha', 'bright', 'dark', 'hard', "retina"
        (for retina displays) and 'simple'. To see icons that will be used for
        each theme check gutter_icon_themes folder in Anaconda package.
    */
    "anaconda_gutter_theme": "basic",

    /*
    Use PyLint instead of PyFlakes and PEP-8

    **** WARNING ****

    - If you set this value as true, PyFlakes and pep8 will not be used.
    - PyLint does *NOT* support linting buffers that are not already saved in
      the file system.

    **** WARNING ****
    */
    "use_pylint": false,

    // Set this to false to turn pep8 checking off completely.
    "pep8": true,

    /*
        If set, the given file will be used as configuration for pep8.

        **** WARNING ****

        - If this option is set to something other than false,
          pep8_ignore and pep8_max_line_length will be silently ignored.

        **** WARNING ****
    */
    // "pep8_rcfile": false,
    "pep8_rcfile": "~/.config/pep8",

    /*
        A list of pep8 error numbers to ignore.
        The list of error codes is in this file:
            https://pycodestyle.readthedocs.io/en/latest/intro.html#error-codes.
        Search for "Ennn:", where nnn is a 3-digit number.
        E309 is ignored by default as it conflicts with pep257 E211
        Can't find E309 in pep8 docs! https://pep8.readthedocs.io/en/release-1.7.x/intro.html
        E123: closing bracket not indented to same level as opening bracket
        E302    expected 2 blank lines, found 0
    */
    "pep8_ignore":
    [
        "E309", "E123", "W504"
    ],

    // Maximum line length for pep8
    "pep8_max_line_length": 130,

    /*
        Default python interpreter

        This can (and should) be overridden by project settings.

        NOTE: if you want anaconda to lint and complete using a remote
        python interpreter that runs the anaconda's minserver.py script
        in a remote machine just use it's address:port as interpreter
        for example:

            "python_interpreter": "tcp://my_remote.machine.com:19360"
    */
    "python_interpreter": "python",   /* "$HOME/anaconda3/bin/python", */

    /*
        HL: Since this ignores Anaconda backend connection errors you might want to
        test your backend connection with:
        import socket; socket.socket(socket.AF_INET, socket.SOCK_STREAM).connect(("localhost", 58551))" */
    "swallow_startup_errors": true,

    /*
        If this is set to true, anaconda will unload plugins that
        make it malfunction interfering with it
    */
    "auto_unload_conflictive_plugins": true,
    /*
        If this is set to true, anaconda will search for environment hook
        files in any directory level down to the one the current file is in
        as opposed to the default where it will only search down to the
        root of your working directory.
    */
    "anaconda_allow_project_environment_hooks": false
}
