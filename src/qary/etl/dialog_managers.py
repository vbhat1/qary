# 1) create doctest in the historybot
# 2) run pytest
# 3) modularise reply method of historybot
# 4) rerun pytests
# 5) doctest for random_reply function
# 6) rerun pytests and debug
# 7) refactor random_reply self to parsed_dialog
# 8) rerun pytests and debug

import random
from nltk.cluster.util import cosine_distance
from qary.spacy_language_model import *
import torch
from transformers import BertModel, BertTokenizer
import numpy as np

# BERT_TOKENIZER = tokenizer_class.from_pretrained(pretrained_weights)
# BERT_MODEL = model_class.from_pretrained(pretrained_weights)
# fix this

    ## modularise this function, into a class and inherit from there
    ## look up 'super()'
    # timeit
    # create a dataset and check accuracies of each model
class Dialog_managers:
    def random_reply(self, statement, dialog_tree_end_bot_statements, dialog_tree_end_state_names, welcome_state_name, default_bot_username, exit_bot_statements, exit_state_turn_dict, compose_statement , context=None):
        r""" Generates random replies for the dialog
        Except for the welcome state, all other states are mere recordings of the quiz responses
        """
        DIALOG_TREE_END_STATE_NAMES = dialog_tree_end_state_names
        DIALOG_TREE_END_BOT_STATEMENTS = dialog_tree_end_bot_statements
        WELCOME_STATE_NAME = welcome_state_name
        DEFAULT_BOT_USERNAME = default_bot_username
        EXIT_BOT_STATEMENTS = exit_bot_statements
        EXIT_STATE_TURN_DICT = exit_state_turn_dict
        if statement in DIALOG_TREE_END_BOT_STATEMENTS:
            statement = None

        # First check to see if we are in the time before the welcome state
        if self.state in DIALOG_TREE_END_STATE_NAMES:
            # First figure out the welcome state name using a magical special WELCOME_STATE_NAME string
            # as the key. This will allow you to access the actual welcome turn
            self.state = self.turns[WELCOME_STATE_NAME]
            self.current_turn = self.turns[self.state]
            index = 0
            if len(self.current_turn['bot']) >= 1:
                index = random.randint(0,len(self.current_turn['bot']) - 1)
            response = compose_statement(self.current_turn['bot'][index])
        else:
            nxt_cndn = self.current_turn['next_condition']

            nxt_cndn_match_mthd_dict = self.get_nxt_cndn_match_mthd_dict(nxt_cndn)
            # for match_method_keyword in ['EXACT', '']
            match_found = False
            for next_state_option in nxt_cndn_match_mthd_dict['EXACT']:
                match_found = self.check_for_match(statement, next_state_option, 'EXACT')
                if match_found:
                    break
            if not match_found:
                for next_state_option in nxt_cndn_match_mthd_dict['LOWER']:
                    match_found = self.check_for_match(statement, next_state_option, 'LOWER')
                    if match_found:
                        break
            if not match_found:
                for next_state_option in nxt_cndn_match_mthd_dict['CASE_SENSITIVE_KEYWORD']:
                    match_found = self.check_for_match(
                        statement, next_state_option, 'CASE_SENSITIVE_KEYWORD'
                    )
                    if match_found:
                        break
            if not match_found:
                for next_state_option in nxt_cndn_match_mthd_dict['KEYWORD']:
                    match_found = self.check_for_match(statement, next_state_option, 'KEYWORD')
                    if match_found:
                        break
            if not match_found:
                for next_state_option in nxt_cndn_match_mthd_dict['NORMALIZE']:
                    match_found = self.check_for_match(statement, next_state_option, 'NORMALIZE')
                    if match_found:
                        break
            if not match_found:
                index_with_highest_ratio = 0
                highest_ratio = 0
                index = 0
                for next_state_option in nxt_cndn_match_mthd_dict['SPACY']:

                    doc1 = nlp(str(statement))
                    doc2 = nlp(str(next_state_option[0]))
                    ratio = doc1.vector.dot(doc2.vector)/np.linalg.norm(doc1.vector)/np.linalg.norm(doc2.vector)
                    if (ratio >= highest_ratio):
                        highest_ratio = ratio
                        index_with_highest_ratio = index
                    index+= 1
                if(highest_ratio> 0.5):
                    self.state = nxt_cndn_match_mthd_dict['SPACY'][index_with_highest_ratio][1]
                    match_found = True
            if not match_found:
                index_with_highest_ratio = 0
                highest_ratio = 0
                index = 0
                if len(statement) > 15:
                    for next_state_option in nxt_cndn_match_mthd_dict['BAG-OF-WORDS']:
                        ratio = sentence_similarity(statement,next_state_option[0], None )
                        if (ratio >= highest_ratio):
                            highest_ratio = ratio
                            index_with_highest_ratio = index
                            index+= 1
                if len(statement) <= 15:
                    for next_state_option in nxt_cndn_match_mthd_dict['BAG-OF-WORDS']:

                        doc1 = nlp(str(statement))
                        doc2 = nlp(str(next_state_option[0]))
                        ratio = doc1.vector.dot(doc2.vector)/np.linalg.norm(doc1.vector)/np.linalg.norm(doc2.vector)
                        if (ratio >= highest_ratio):
                            highest_ratio = ratio
                            index_with_highest_ratio = index
                            index+= 1
                if(highest_ratio> 0.55):
                    self.state = nxt_cndn_match_mthd_dict['BAG-OF-WORDS'][index_with_highest_ratio][1]
                    match_found = True
            if not match_found:
                model_class = BertModel
                tokenizer_class = BertTokenizer
                pretrained_weights = 'bert-base-multilingual-cased'
                tokenizer = tokenizer_class.from_pretrained(pretrained_weights)
                model = model_class.from_pretrained(pretrained_weights)

                index_with_highest_ratio = 0
                highest_ratio = 0
                index = 0
                for next_state_option in nxt_cndn_match_mthd_dict['BERT']:

                    input_ids = torch.tensor([tokenizer.encode(str(statement))])
                    with torch.no_grad():
                        output_tuple = model(input_ids)
                        last_hidden_states = output_tuple[0]


                    input_ids1 = torch.tensor([tokenizer.encode(str(next_state_option[0]))])
                    with torch.no_grad():
                        output_tuple1 = model(input_ids1)
                        last_hidden_states1 = output_tuple1[0]


                    ratio = (last_hidden_states[0][0].dot(last_hidden_states1[0][0])/np.linalg.norm(last_hidden_states[0][0])/np.linalg.norm(last_hidden_states1[0][0])).item()
                    if (ratio >= highest_ratio):
                        highest_ratio = ratio
                        index_with_highest_ratio = index
                    index+= 1
                if(highest_ratio> 0.5):
                    self.state = nxt_cndn_match_mthd_dict['BERT'][index_with_highest_ratio][1]
                    match_found = True
            if not match_found:
                self.state = nxt_cndn_match_mthd_dict[None][0][1]



            self.current_turn = self.turns.get(self.state, EXIT_STATE_TURN_DICT)
            index = 0
            if len(self.current_turn.get(DEFAULT_BOT_USERNAME, EXIT_BOT_STATEMENTS)) >= 1:
                index = random.randint(0,len(self.current_turn.get(DEFAULT_BOT_USERNAME, EXIT_BOT_STATEMENTS)) - 1)
            response = compose_statement(self.current_turn.get(DEFAULT_BOT_USERNAME, EXIT_BOT_STATEMENTS)[index])


        return [(1.0, response)]

    # sentence transformers
    # create BERT vector
    # create your own cosine distance function


def sentence_similarity(sent1, sent2, stopwords=None): # incprporate nlp and .text
                                                        # bag of words similarity
    if stopwords is None:
        stopwords = []

    sent1 = [w.lower() for w in sent1]
    sent2 = [w.lower() for w in sent2]

    all_words = list(set(sent1 + sent2))

    vector1 = [0] * len(all_words)
    vector2 = [0] * len(all_words)


    for w in sent1:
        if w in stopwords:
            continue
        vector1[all_words.index(w)] += 1


    for w in sent2:
        if w in stopwords:
            continue
        vector2[all_words.index(w)] += 1

    return 1 - cosine_distance(vector1, vector2)
