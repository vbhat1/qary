from graphviz import Digraph
from pathlib import Path

from qary.etl.dialog import TurnsPreparation, load_dialog_turns
from qary.etl.dialog import DEFAULT_QUIZ


def dumps(graph, format='svg', quiet=True):
    """ Dump a graphviz Graph (or Digraph) object to a unicode string """
    return graph.pipe(format=format, quiet=quiet).decode()


def graph_dialog_turns(dialog_turns=None):
    if dialog_turns is None:
        dialog_turns = DEFAULT_QUIZ
    if isinstance(dialog_turns, (Path, str)) and Path(dialog_turns).is_file():
        dialog_turns = load_dialog_turns


def draw_convoscript(graph=None, dialog_turns=None, name='qary.visualizations.draw_convoscript'):
    """ Display a finite statem machine diagram of the conversation dialog graph

    >>> draw_convoscript()
    <graphviz.dot...
    """
    if graph is None:
        if isinstance(dialog_turns, (str, Path)) and Path(dialog_turns).is_file():
            dialog_turns = TurnsPreparation(turns_list=dialog_turns)
        if dialog_turns is not None:
            graph = graph_dialog_turns(dialog_turns)
    if graph is None:
        g = Digraph(name, filename=None)
    g.attr(rankdir='LR', size='8,5')

    g.attr('node', shape='doublecircle')
    g.node('LR_0')
    g.node('LR_3')
    g.node('LR_4')
    g.node('LR_8')

    g.attr('node', shape='circle')
    g.edge('LR_0', 'LR_2', label='SS(B)')
    g.edge('LR_0', 'LR_1', label='SS(S)')
    g.edge('LR_1', 'LR_3', label='S($end)')
    g.edge('LR_2', 'LR_6', label='SS(b)')
    g.edge('LR_2', 'LR_5', label='SS(a)')
    g.edge('LR_2', 'LR_4', label='S(A)')
    g.edge('LR_5', 'LR_7', label='S(b)')
    g.edge('LR_5', 'LR_5', label='S(a)')
    g.edge('LR_6', 'LR_6', label='S(b)')
    g.edge('LR_6', 'LR_5', label='S(a)')
    g.edge('LR_7', 'LR_8', label='S(b)')
    g.edge('LR_7', 'LR_5', label='S(a)')
    g.edge('LR_8', 'LR_6', label='S(b)')
    g.edge('LR_8', 'LR_5', label='S(a)')

    g.format = 'svg'
    g.dumps = dumps
    return g
