-
  teacher: "So, you need to write a personal essay for your college application, huh?"
  student: "Yup."
-
  teacher: "What is your assignment? Do you need a personal essay or story for anything?"
  student: "Yes"
-
  teacher: "Do you know what you’d like to write?"
  student: "No."
-
  teacher: "Well, the people reading it want to understand you better. What do you want them to know about you?"
  student: "I don’t know."
-
  teacher: "Are you applying to schools where you really want to get accepted?"
  student: "Yeah, a few."
-
  teacher: "Well, a lot of people are applying, so you need to differentiate yourself somehow."
  student: "Okay."
-
  teacher: "Picture yourself standing in a long line of people who look almost identical to you. That’s what it’s like when admissions officers are looking at applications. Your personal essay is your chance to step out of the line, wave your hand, and say, “This is what makes me different...”"
  student: "Okay."
-
  teacher: "So, what makes you different?"
  student: "I don’t know."
-
  teacher: "Let me approach this from a different angle: what are you passionate about?"
  student: "I like soccer."
-
  teacher: "So, you are passionate about soccer?"
  student: "Yes."
-
  teacher: "Why?"
  student: "I like playing it."

-
  teacher: "So do a lot of people. What do you like most about soccer?"
  student: "I don’t know, just everything."
-
  teacher: "What would you miss most about it, if you stopped playing?"
  student: "Being part of the team."
-
  teacher: "Okay, see, now we’re making progress! What do you like about being part of the team?"
  student: "I don’t know, it’s just more fun than playing an individual sport because you get to share everything."
-
  teacher: "So, it’s more fun to celebrate victories?"
  student: "Yeah, but it also helps when you lose because you can lean on each other."
-
  teacher: "Good point. What makes a good teammate?"
  student: "You have to put the team first. You can’t be selfish. You need to support everyone else on the team."
-
  teacher: "How does a good teammate support his or her teammates?"
  student: "Well, in a game, it means understanding the responsibilities of your position so that you can help the team execute. Off the field, it means having a good attitude, working hard, and doing things together."
-
  teacher: "Are you a good teammate?"
  student: "Yeah, I think so."
-
  teacher: "Good to hear. Have you learned anything about being a good soccer teammate that you’ve applied to other aspects of your life?"
  student: "Yeah, a lot."
-
  teacher: "Like what?"
  student: "Everything I mentioned."
-
  teacher: "So, you try to have a good attitude and work hard?"
  student: "Most of the time."
-
  teacher: "Would you like someone reading your application to know that you like soccer, and that you’ve learned things from being part of the team -- like having a good attitude and working hard -- that you now apply to other aspects of your life?"
  student: "Yes."
-
  teacher: "Then, let’s make that the focus of your personal essay, okay?"
  student: "Okay."
-
  teacher: "So, let’s create a statement that captures that idea. Give it a try."
  student: "Soccer has taught me a lot of things that are useful in life."
-
  teacher: "Okay, good start. What did you tell me about why you’re passionate about soccer?"
  student: "I like being part of the team."
-
  teacher: "Right, so let’s weave that into your statement. Try again."
  student: "Soccer has taught me to be a good teammate and I’ve applied these lessons to other parts of my life."
-
  teacher: "Excellent. Now that you’ve established your main point, the rest of your essay should be all about supporting it with examples."
  student: "Okay, I get it."
-
  teacher: "So, what lessons about being a good teammate do you want to focus on?"
  student: "Having a good attitude and working hard."
-
  teacher: "Great. So, give me an example of why having a good attitude in soccer is important."
  student: "Well, if you don’t have a good attitude at practice, then other members of the team may start to have bad attitudes too. And if that happens, then the practice isn’t going to go well, which means the team is probably not going to play well in the next game. Or, if it’s just you with the bad attitude, then the coach probably isn’t going to play you."
-
  teacher: "Got it. So, how has this lesson translated to your life?"
  student: "Well, I try to bring a good attitude to my classes in school. Like, when kids are not paying attention and the teacher is getting annoyed, I’ll sometimes say something."
-
  teacher: "Excellent. Do you see how these examples support your main statement?"
  student: "Yes."
-
  teacher: "Good. So, now let’s do the same thing for working hard. What’s important about working hard in soccer?"
  student: "Well, if you don’t work hard, you’re not going to improve. Like, my ball skills weren’t very good when I was in ninth grade, but I worked hard on them and, by senior year, I was very confident with my ability to control the ball in games."
-
  teacher: "Great. How has that translated to the rest of your life?"
  student: "It’s the same as my classes. Some subjects, like math, come pretty easily to me. But I’m not very good at writing, so I have had to work at it to get better."
-
  teacher: "How have you worked harder?"
  student: "In ninth grade, I wasn’t a good writer at all. But, in tenth grade, I started submitting drafts to my teacher, who would give me feedback. That helped a lot, so I have continued to do that."
-
  teacher: "Excellent. Do you feel like this example does a good job of supporting your main point?"
  student: "Yes."
-
  teacher: "Good. So, now you have your two supporting examples. Anything else you’d like the admissions officers to know about you?"
  student: "Well, we went to the conference championship senior year."
-
  teacher: "Great, so maybe you could add that in to show that sometimes having a good attitude and working hard can pay off. Do you think it would make a good conclusion?"
  student: "Probably."
-
  teacher: "What would be the equivalent in your classes of winning the championship in soccer?"
  student: "Maybe getting into college."
-
  teacher: "I like it. Do you want to end your essay with that?"
  student: "Yes."
-
  teacher: "So, you’ve established your main point, provided some examples to support it, and come up with a good conclusion. Any idea how you’d like to start your essay?"
  student: "I’m not sure."
-
  teacher: "Remember where we started: you said soccer is something you’re passionate about. So, maybe start there."
  student: "Okay."
